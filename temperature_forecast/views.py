import re
from datetime import datetime

import requests
from django.shortcuts import render
# This package was needed to user the django cache system, so the user will not need to make a request to the
# API every time he needs it.
from django.core.cache import cache
from django.views.generic import TemplateView
from django.conf import settings


class ShowTemperatureView(TemplateView):
    """
    This view will be responsible to handle the user's input, consulting the API for
    temperature previsions based on latitude and longitude.
    """
    template_name = 'show_temperatures.html'

    def post(self, request):
        # Here I am rounding the user input for 3 decimal places
        latitude = round(float(request.POST.get('latitude_input')), 3)
        longitude = round(float(request.POST.get('longitude_input')), 3)
        temperature_output = cache.get(f'{latitude}/{longitude}')
        if temperature_output:
            context = {
                'latitude': latitude,
                'longitude': longitude,
                'temperature_output': int(round(temperature_output))
            }
            return render(request, self.template_name, context)
        return self.request_to_darksky_api(request, latitude, longitude)

    def request_to_darksky_api(self, request, latitude, longitude):
        """
        This function is responsible to get the temperature for the current moment based on the user
        input for latitude and longitude.
        :param request: The request view object
        :param latitude: The latitude user input
        :param longitude: The longitude user input
        :return: Returns the temperature for a given latitude and longitude for the current moment
        """
        try:
            response = requests.get(
                f'https://api.darksky.net/forecast/{settings.DARK_SKY_API_KEY}/{latitude},{longitude}?units=si'
            )
            json_object = response.json()
            if 'error' in json_object and json_object['error']:
                context = {
                    'code': json_object['code'],
                    'error': json_object['error'],
                }
                return render(request, self.template_name, context)
            temperature_output = json_object['currently']['temperature']
            cache.set(f'{latitude}/{longitude}', temperature_output)
            context = {
                'latitude': latitude,
                'longitude': longitude,
                'temperature_output': temperature_output
            }
            return render(request, self.template_name, context)
        except requests.exceptions.Timeout:
            return render(request, self.template_name, {'error': 'Timeout error, try again later!'})
        except requests.exceptions.TooManyRedirects:
            return render(request, self.template_name, {'error': 'TooManyRedirects error, the link may be broken!'})
        except requests.exceptions.RequestException as e:
            print(e)
            return render(request, self.template_name, {'error': 'Unexpected error. Please try again later!'})


class ShowTemperatureListView(TemplateView):
    """
    This view will be responsible to handle the user's input, consulting the API for
    hourly temperature previsions based on a custom address data.
    """
    template_name = 'show_temperatures_list.html'

    def post(self, request):
        """
        This is the function that came from the generic view. Here I handle the user input, filtering every
        desired data to send to the user
        :param request: Request method
        :return: Returns the temperature lise for a given location or an error.
        """
        # Here I am rounding the user input for 3 decimal places
        location = self.select_location(request.POST.get('location_input'))
        if not cache.get(f'{location}'):
            google_api_response = self.request_for_location_to_google_api(location)
            if type(google_api_response) == tuple:
                latitude, longitude, city, state = google_api_response
            else:
                return render(request, self.template_name, google_api_response)
        else:
            latitude, longitude, city, state = cache.get(f'{location}')
        temperature_list = cache.get(f'{latitude}/{longitude}')
        if temperature_list:
            context = {
                'location': location.replace('+', ' '),
                'temperature_list_output': temperature_list,
                'city': city,
                'state': state
            }
            return render(request, self.template_name, context)
        return self.request_to_darksky_api(request, latitude, longitude, city, state, location)

    def select_location(self, location):
        """
        This function is responsible for handle the location input
        :param location: The location provided by the user
        :return: Returns a string object in the desired format
        """
        zip_code_filtered = ''.join(filter(lambda x: x, location.split(' ')))
        zip_code = re.match(
            r'^\d{5}/\d{3}$', zip_code_filtered
        ) or re.match(
            r'^\d{5}-\d{3}$', zip_code_filtered
        ) or re.match(
            r'^\d{8}$', zip_code_filtered
        )
        if zip_code:
            zip_code = zip_code.string.replace('/', '').replace('-', '')
        return zip_code or '+'.join(filter(lambda x: x, location.split(' ')))

    def request_for_location_to_google_api(self, location):
        """
        This function is responsible for getting the user location, converting it into longitude and latitude values
        :param location: The location provided by the user
        :return: Returns a tuple of (latitude, longitude)
        """
        try:
            response = requests.get(
                f'https://maps.googleapis.com/maps/api/geocode/json?address={location}&key={settings.GOOGLE_API_KEY}'
            )
            json_object = response.json()
            if json_object['status'] != 'OK':
                # Doing tests I came across the problem that the Google API may not find the zipcode
                # by searching the format XXXXXXX, so I decided to make this "if" to test again in the format XXXXX-XXX
                if re.match(r'^\d{8}$', location):
                    new_location = f'{location[:5]}-{location[5:]}'
                    response = requests.get(
                        f'https://maps.googleapis.com/maps/api/geocode/json?'
                        f'address={new_location}&key={settings.GOOGLE_API_KEY}'
                    )
                    json_object = response.json()
                if json_object['status'] != 'OK':
                    context = {
                        'error': f"{json_object['status']}, please check your input value. "
                        f"If you are trying to insert a zipcode, "
                        f"make sure it has these shapes: XXXXX-XXX, XXXXX/XXX or XXXXXXXX",
                    }
                    return context
            latitude = round(float(json_object['results'][0]['geometry']['location']['lat']), 3)
            longitude = round(float(json_object['results'][0]['geometry']['location']['lng']), 3)
            city = 'City not found'
            state = 'State not found'
            # Here I am iterating over the 'address_components' key to get the city and the state. It is assumed that
            # the city will always be under the "administrative_area_level_2" key and the state
            # under  the "administrative_area_level_1" key.
            for area_dict in json_object['results'][0]['address_components']:
                if 'administrative_area_level_2' in area_dict['types']:
                    city = area_dict['long_name']
                elif 'administrative_area_level_1' in area_dict['types']:
                    state = area_dict['long_name']
            # Coordinates storage for one day, so the user does not need to send
            # a request to GoogleAPI every time he needs it.
            cache.set(f'{location}', (latitude, longitude, city, state), timeout=60*60*24)
            return latitude, longitude, city, state
        except requests.exceptions.Timeout:
            return {'error': 'Timeout error, try again later!'}
        except requests.exceptions.TooManyRedirects:
            return {'error': 'TooManyRedirects error, the link may be broken!'}
        except requests.exceptions.RequestException as e:
            print(e)
            return {'error': 'Unexpected error. Please try again later!'}

    def request_to_darksky_api(self, request, latitude, longitude, city, state, location):
        """
        This function is responsible to get the temperature for the current moment based on the user
        input for latitude and longitude.
        :param request: The request view object
        :param latitude: The latitude user input
        :param longitude: The longitude user input
        :param city: The city given by GoogleAPI
        :param state: The state given by GoogleAPI
        :param location: The longitude user input
        :return: Return the temperature for a given latitude and longitude for the current moment
        """
        try:
            response = requests.get(
                f'https://api.darksky.net/forecast/{settings.DARK_SKY_API_KEY}/{latitude},{longitude}?units=si'
            )
            json_object = response.json()
            if 'error' in json_object and json_object['error']:
                context = {
                    'code': json_object['code'],
                    'error': json_object['error'],
                }
                return render(request, self.template_name, context)
            # It was required to give the maximum and minimum for the next 6 hours, but the DarkSky API does not
            # provide such data. The solution was to give the user the average temperature data for that hour.
            temperature_list_output = {
                f"{datetime.fromtimestamp(val['time']).hour}:00" if i else 'Current': int(round(val['temperature'], 0))
                for i, val in enumerate(json_object['hourly']['data'][:6])
            }
            # As the prevision is given from hour to hour, it would be better for the user to cache the data for
            # one hour. If the user tries to get the prevision at 12 p.m. he would get a list like [12:00, 13:00, ...].
            # If the user tries to get the prevision one hour later, it would be 1 p.m. and, as the data is stored
            # in the cache for one hour, he would get [12:00, 13:00, ...] like before. So I decided to change the
            # "timeout" so that will not happen anymore.
            cache.set(f'{latitude}/{longitude}', temperature_list_output, timeout=60*60)
            context = {
                'location': location.replace('+', ' '),
                'temperature_list_output': temperature_list_output,
                'city': city,
                'state': state
            }
            return render(request, self.template_name, context)
        except requests.exceptions.Timeout:
            return render(request, self.template_name, {'error': 'Timeout error, try again later!'})
        except requests.exceptions.TooManyRedirects:
            return render(request, self.template_name, {'error': 'TooManyRedirects error, the link may be broken!'})
        except requests.exceptions.RequestException as e:
            print(e)
            return render(request, self.template_name, {'error': 'Unexpected error. Please try again later!'})
