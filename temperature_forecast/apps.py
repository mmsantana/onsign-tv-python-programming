from django.apps import AppConfig


class TemperatureForecastConfig(AppConfig):
    name = 'temperature_forecast'
