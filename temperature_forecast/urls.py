from django.urls import path
from .views import ShowTemperatureView, ShowTemperatureListView

urlpatterns = [
    path('latitude_longitude/', ShowTemperatureView.as_view(), name='show_temperatures'),
    path('', ShowTemperatureListView.as_view(), name='show_temperatures_list'),
]
